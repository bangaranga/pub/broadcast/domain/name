# name - domain

## list

 ***no specific order***

***

### base
https://arstechnica.com/               
https://www.theverge.com/              
https://www.theregister.com/               
https://9to5mac.com/                   
https://old.reddit.com/                
https://www.troddit.com/                             
https://teddit.net/                            
https://twitter.com/                            
https://nitter.net/                            
https://woshub.com/              
https://www.google.com/                 
https://superuser.com/                
https://oisd.nl/                   
https://disroot.org/                     
https://github.com/                 
https://gitlab.com/                        
https://rethinkdns.com/                       
https://www.plex.tv/                            
https://wiki.archlinux.org/                      
https://wiki.installgentoo.com/                           
https://wiki.debian.org/                       
https://peter.sh/experiments/chromium-command-line-switches/                             
https://chromium.woolyss.com/                           
https://tosdr.org/en/frontpage                                  
https://reports.exodus-privacy.eu.org/en/                               
https://www.supermicro.com/wdl/                                        
https://ftp.mozilla.org/pub/                               
https://chromium.cypress.io/                             
https://www.majorgeeks.com/                   


***


### path
https://chromium.cypress.io/win64/                                       
https://www.supermicro.com/wdl/driver/                                        
https://ftp.mozilla.org/pub/firefox/releases/                               
https://kb.adguard.com/en/general/how-to-create-your-own-ad-filters                                   
https://nitter.net/nickhintonn                               
https://github.com/bkerler/oppo_decrypt                                        
https://github.com/italorecife/OppoRealme-OFP-Flash/releases/                                      
https://github.com/Nuitka/Nuitka                                      
https://github.com/rotemdan/ExportCookies                                 
https://old.reddit.com/r/buildapcsales/                                      
https://admin.google.com/ac/chrome/settings/user/bookmarks/                                       
https://developer.nvidia.com/video-encode-and-decode-gpu-support-matrix-new                           
https://vivaldi.com/blog/releases/                                   
https://github.com/cipher387/Dorks-collections-list                                  
https://github.com/cipher387/Advanced-search-operators-list                                
https://www.catalog.update.microsoft.com/Search.aspx?q=intel%20precise%20touch%20device              
https://get.google.com/albumarchive/                      




